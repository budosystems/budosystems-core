.. image:: https://img.shields.io/website-up-down-green-red/http/shields.io.svg
    :target: http://shields.io/


.. image:: https://img.shields.io/badge/docs-${SHIELDS_COMMIT_REF}-blue
    :target: https://budosystems.readthedocs.io/en/${CI_COMMIT_REF_SLUG}

.. image:: https://gitlab.com/budosystems/budosystems-core/badges/${CI_COMMIT_REF_NAME}/coverage.svg
    :target: ${CI_PROJECT_URL}/-/graphs/${CI_COMMIT_REF_NAME}/charts

.. image:: https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/pylint/score.svg?job=pylint

.. image:: https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/artifacts/${CI_COMMIT_REF_NAME}/raw/mypy/mypy-score.svg?job=mypy
