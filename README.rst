..
  Generated content, do not update

.. image:: https://img.shields.io/website-up-down-green-red/http/shields.io.svg
    :target: http://shields.io/


.. image:: https://img.shields.io/badge/docs-EXPERIMENT--submodule-blue
    :target: https://budosystems.readthedocs.io/en/experiment-submodule

.. image:: https://gitlab.com/budosystems/budosystems-core/badges/EXPERIMENT-submodule/coverage.svg
    :target: https://gitlab.com/budosystems/budosystems-core/-/graphs/EXPERIMENT-submodule/charts

.. image:: https://gitlab.com/api/v4/projects/21497171/jobs/artifacts/EXPERIMENT-submodule/raw/pylint/score.svg?job=pylint

.. image:: https://gitlab.com/api/v4/projects/21497171/jobs/artifacts/EXPERIMENT-submodule/raw/mypy/mypy-score.svg?job=mypy


Budo Systems
============
Budo Systems is an Open Source Martial Arts Studio Management System.
This project (Budo Systems Core) is a storage-agnostic and presentation-agnostic
core implementation of the most needed models and support utilities to run a
martial arts studio.

All the code is currently in Python.

Storage-specific and view-specific implementations will be done in separate projects.

Checkout the `Budo Systems GitLab group <https://gitlab.com/budosystems>`_
to see all the active projects.

If you're interested in helping out, please let me know.



What's the status of this project?
----------------------------------
This project is very much in its infancy.
The hope is for it to quickly grow into something that will be usable by
many martial arts studios, and perhaps even other similar types of services.
