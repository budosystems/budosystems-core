

Budo Systems
============
Budo Systems is an Open Source Martial Arts Studio Management System.
This project (Budo Systems Core) is a storage-agnostic and presentation-agnostic
core implementation of the most needed models and support utilities to run a
martial arts studio.

All the code is currently in Python.

Storage-specific and view-specific implementations will be done in separate projects.

Checkout the `Budo Systems GitLab group <https://gitlab.com/budosystems>`_
to see all the active projects.

If you're interested in helping out, please let me know.



What's the status of this project?
----------------------------------
This project is very much in its infancy.
The hope is for it to quickly grow into something that will be usable by
many martial arts studios, and perhaps even other similar types of services.
