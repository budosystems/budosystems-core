Frequently Asked Questions
==========================

As of the last update of this FAQ, this project only has one contributor, i.e. project's originator, Joël Larose.
Much of this FAQ is written in the first person from his perspective.

What is Budo Systems?
---------------------

Budo Systems is an Open Source Martial Arts Studio Management System.  This project (Budo Systems Core) is a
storage-agnostic and presentation-agnostic core implementation of the most needed models and support utilities to run a
martial arts studio.

All the code is currently in Python.

Storage-specific and view-specific implementations will be done in separate projects.

Why is this needed?
-------------------
As a martial arts instructor, I've tried many systems for managing my studio.  Most of them
did many things "right" (i.e. in a way that worked for me), many other things "wrong"
(i.e. in a way that I didn't agree with), and some things that I really didn't need, and didn't implement
features I needed.

These were all paid services.  And I hated paying for something I thought I could implement better.

Turns out it's not quite as easy as I'd anticipated, but I love the challenge.

Why make it Open Source?
------------------------
I've long been the benefactor of Open Source software.  I want to share my own expertise with the OSS community.
I also recognize my own limitations in building an ambitious project like this, and I would appreciate the
collaboration of other like-minded people.

Who are you?
------------

Joël Larose
~~~~~~~~~~~
My name is Joël Larose.  I have a wide variety of skills and interests.  Most relevant to this project:

- I have a degree in Computer Science
- I have a black belt in karate
- I run my own karate school

This project allows me to combine my often-unrelated skills.

Other Contributors
~~~~~~~~~~~~~~~~~~
Once we have more contributors, they can add their short bio here.

What's the status of this project?
----------------------------------
This project is very much in it's infancy.  The hope is for it to quickly grow into something that will be usable by
many martial arts studios, and perhaps even other similar types of services.

