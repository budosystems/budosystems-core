.. Budo Systems documentation master file, created by
   sphinx-quickstart on Fri Dec  4 05:56:34 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. contents:: Contents
   :local:


.. include:: ../../README.rst

Documentation
-------------

.. toctree::
  :maxdepth: 1

  FAQ
  design
  Contributing
  CodeOfConduct
  api

Change Log
----------
.. toctree::
   CommitLog


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
