### Overview
Briefly describe the nature of the bug you're submitting.

### Steps to Reproduce
What are you trying to do?  Include some sample code.

### Current Bug Behaviour
What results are you getting?  Include any relevant error messages.

### Expected Correct Behaviour
What results are you expecting?

### Environment
- **OS**: 
- **Python version**:
- **Budo Systems Core version**:
- **Other packages**:
- **Other relevant information**:

### Discussion
Other thoughts you may have on the matter.


/label ~bug
